from setuptools import setup


DESC = "Python invocation logging via a Met Office specific sitecustomize.py"

setup(name="mo_sitecustomize",
      version="0.1.5",
      author="AVD",
      author_email="ml-avd@metoffice.gov.uk",
      url="https://exxgitrepo:8443/projects/AVD/repos/mo_sitecustomize/browse",
      py_modules=["sitecustomize"],
      description=DESC
     )
