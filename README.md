# mo_sitecustomize

A `sitecustomize.py` for a Scientific Software Stack environment.


## What does it do?

This `sitecustomize.py` currently only provides Python invocation logging for stack environments.
It logs both Python startup (invocation) and shutdown. Startup and shutdown are
connected by sharing a hash that is unique to the Python invocation.

Logging is achieved with the Python `logging` standard library. Logged information
is sent via http to a log server running in a daemonised process on `exvavddoc01`.


## What does it log?

On startup:

* the string "startup", indicating this logged information pertains to Python startup,
* a unique 16 character hash for this Python session,
* the username that invoked the Python session,
* the hostname on which the Python session was invoked,
* the version of the Python that was invoked,
* the full path to the Python executable that was run,
* command line invocation of Python process, and
* command line invocatoin of Python process parent.

On shutdown:

* the string "shutdown", indicating this logged information pertains to a Python invocation
being shut down,
* the unique 16 character hash for this Python session, and
* any Python libraries of interest that have been imported.

Libraries of interest are limited and recorded in `sitecustomize.py`. If none
of these libraries are imported, `None` is logged instead.
