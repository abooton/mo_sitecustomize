import atexit
import datetime
import getpass
import hashlib
import logging
from logging.handlers import HTTPHandler, DEFAULT_HTTP_LOGGING_PORT
from multiprocessing import Process
import os
import re
import socket
import sys


# Logging server settings.
LOGGING_SERVER = 'exvavddoc01'
PORT = DEFAULT_HTTP_LOGGING_PORT  # Tornado web-server port
SSS_ENV_VAR = "SSS_LOG_DISABLE"
TIMEOUT = 1.0  # In seconds
URL = '/'

# Define target packages imported by the user to be logged.
LIBS_OF_INTEREST = set(['biggus', 'cartopy', 'cf_units', 'iris', 'IPython',
                        'jupyter_core', 'matplotlib', 'numpy', 'scipy',
                        'dask'])


def check_server_available():
    """Check whether the log server is available."""
    try:
        address = (LOGGING_SERVER, PORT)
        connection = socket.create_connection(address, timeout=TIMEOUT)
        connection.close()
        result = True
    except:
        result = False

    return result


def check_active_logging():
    """
    Check whether we need to disable logging for this invocation.

    Python invocation logging can be disabled by exporting the environment
    variable defined by SSS_ENV_VAR to any value.

    Returns:

        * `True` if the environment variable is not set i.e., logging enabled.
        * `False` if the environment variable is set i.e., logging disabled.

    """
    env_var_value = os.getenv(SSS_ENV_VAR)
    # `os.getenv` returns `None` by default if `env_var_name` is not set.
    # If `env_var_name` is set then we want to disable logging.
    result = env_var_value is None

    return result


def perform_logging():
    """
    Determine whether the client should send a log to the server.

    """
    return check_active_logging() and check_server_available()


def get_cmdline(pid, default=None):
    """
    Capture the invocation sequence of the process.

    This is supported by the /proc/PID/cmdline details available
    from the Linux kernel for each user process.

    Args:
        * pid - the process ID

    Kwargs:
        * default - substitute value for unavailable invocation details

    Returns:
        * (string) The specified process ID invocation sequence.

    """
    result = default
    fname = '/proc/{}/cmdline'.format(pid)
    if os.path.isfile(fname):
        try:
            with open(fname, 'rb') as fi:
                cmdline = fi.read().decode('utf-8')
                # Replace whitespace and kernel injected \x00 characters
                # to assist the notebook pandas post-processing, which
                # splits a dataframe series column on whitespaces.
                result = re.sub('[\s\x00]+', '^', cmdline).strip('^')
        except:
            pass
    return result


def log_invoc_start():
    """Logs a Python invocation."""
    # Get info: executing user, hostname, Python version, path to Python exe,
    #           process cmdline invocation, and parent process cmdline
    #           invocation.
    username = getpass.getuser()
    hostname = socket.gethostname()
    version = '{v.major}.{v.minor}.{v.micro}'.format(v=sys.version_info)
    exe = sys.executable
    cmdline = get_cmdline(os.getpid())
    pcmdline = get_cmdline(os.getppid())

    # Log the invocation.
    info_str = ('startup {ref} {user} {host} {py_ver} {py_exe} '
                '{cmdline} {pcmdline}')
    LOGGER.info(info_str.format(ref=INVOC_REF, user=username, host=hostname,
                                py_ver=version, py_exe=exe, cmdline=cmdline,
                                pcmdline=pcmdline))


def filter_sort_imports(imports):
    """
    Filter and sort all imports in the logged Python invocation to return a
    sorted list of only libraries of interest that appear in all the imports.

    """
    all_imports = imports - INITIAL_IMPORTS
    filtered_imports = all_imports & LIBS_OF_INTEREST
    if len(filtered_imports):
        result = ','.join(i for i in sorted(list(filtered_imports)))
    else:
        result = None
    return result


@atexit.register
def log_invoc_end_and_imports():
    """
    Logs the end of a Python invocation, including determining library imports
    during the invocation. Decorator means it runs on exit only.

    """
    if perform_logging():
        end_info_str = 'shutdown {ref} {imports}'
        imports = set([k.split('.')[0] for k in sys.modules.keys()])
        filtered_imports = filter_sort_imports(imports)
        LOGGER.info(end_info_str.format(ref=INVOC_REF,
                                        imports=filtered_imports))


if perform_logging():
    # Get a unique ref for this invocation.
    salt_dt = datetime.datetime.isoformat(datetime.datetime.now())
    salt_pid = os.getpid()
    salt_hnm = socket.gethostname()
    salt = '{}{}{}'.format(salt_dt, salt_pid, salt_hnm)
    INVOC_REF = hashlib.sha256(salt.encode('utf-8')).hexdigest()[:16]

    # Get starting state of modules imported.
    INITIAL_IMPORTS = set([k.split('.')[0] for k in sys.modules.keys()])

    # Set up logger.
    logger_name = 'py_invoc'
    LOGGER = logging.getLogger(logger_name)
    LOGGER.setLevel(logging.INFO)
    LOGGER.propagate = False
    http_handler = HTTPHandler('{}:{}'.format(LOGGING_SERVER, PORT),
                               URL,
                               method='POST')
    LOGGER.addHandler(http_handler)

    # Run logging in a separate process so it is non-blocking.
    # Avoid use of threading, which leads to a race condition in
    # Python3 when the main process imports requests.
    Process(target=log_invoc_start).start()
